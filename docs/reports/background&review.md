# Background and Literature review


## 1. High Performance Computing (HPC) Services and User Support

### 1.1 The background and importance of HPC services

High-performance computing (HPC) services play a vital role in advancing scientific research and industrial applications by leveraging the combined capabilities of CPU and GPU clusters to process and analyze large-scale data sets. This integration allows complex computational tasks to be performed at speeds unreachable by standard computers, enabling breakthroughs in fields as diverse as climate modeling, genetic research, and aerospace engineering [1].

The importance of HPC extends beyond computing speed. It can handle big data analytics, which is critical to driving innovation and making informed decisions in today's data-driven world. Furthermore, HPC is crucial in the medical field, where it facilitates the development of new drugs and the analysis of high-resolution medical images [2], thereby improving diagnosis and patient care. The concepts and technology applications of "HPC+" are increasingly penetrating and integrating into key technical fields of various industries, significantly improving the capabilities and efficiency of various industries. 

### 1.2 HPC user support

Just as the demand for "HPC+" services continues to grow in various industries, the demand for HPC user support services has also increased. Faced with the shortcomings of current user support services, especially in the face of high growth and high concurrent user needs, it has become particularly important to train a specialized large language model (LLM) to provide user support. This model can automatically handle a large number of user queries, which not only improves response speed and service efficiency, but also ensures stable support when user demand surges.

In addition, with the advancement of AI and machine learning technology, these models can continuously learn from new user interactions, continuously optimizing their performance and the relevance of answers. Therefore, using LLM to support high-growth and high-concurrency user needs is not only an effective way to solve the current shortage of user support services, but also an important step in promoting the development of HPC services to be more efficient and intelligent.

## 2. Machine Learning and Large Language Models (LLM)

### 2.1 Basic knowledge background

#### 2.1.1 Machine Learning

Machine learning is a technique that enables computers to improve themselves through experience, without the need for explicit programming instructions[3]. It is a subfield of artificial intelligence that aims to develop algorithms that enable computers to learn from data and make decisions or predictions.

Machine learning algorithms are generally divided into three categories: supervised learning, unsupervised learning, and reinforcement learning. Supervised learning algorithms learn by training data sets containing inputs and expected outputs. Unsupervised learning discovers the inherent patterns and structures of the data by analyzing unlabeled data, while reinforcement learning guides the model to make optimal decisions through reward and punishment mechanisms.[4]


(1) Supervised learning 

As shown in the figure below, in supervised learning, the algorithm uses example input data with explicit outputs (i.e., training data) to learn how to map inputs to outputs. This process involves the model being trained repeatedly, comparing its predictions with actual results, and self-correcting until it reaches high accuracy. Supervised machine learning algorithms can predict future events by applying knowledge learned in the past to new data using labeled data examples. 
![Supervised learning](./photos/supervisedLearning.png "Supervised Learning")
A supervised learning problem can take the following form:
- Classification: It is an output that belongs to a specific category such as classifying an email as spam or not spam. This type of task usually uses pre-labeled data to train the model and uses algorithms such as support vector machines, naive Bayes, and k-nearest neighbors to construct decision boundaries to achieve classification.

- Regression: It aims to predict the output of continuous numerical values, such as house prices or temperature. The main difference from classification is that the output of regression is continuous values rather than discrete categories. Commonly used regression models include linear regression, polynomial regression and support vector regression.

- Forecasting: A method of making predictions based on past and present data. This is also called time series forecasting [4].

(2)Unsupervised learning

Unsupervised learning is another way of machine learning that does not rely on pre-labeled data. The process is shown in the figure below. This approach allows the algorithm to explore the input data on its own, trying to understand the underlying structure and patterns of the data. Since in unsupervised learning, the data has no label or category information attached to it, the algorithm must figure out how the data is organized on its own, such as by clustering similar data points, finding outliers, or otherwise understanding the characteristics of the data set. This learning model is particularly suitable for data exploration and discovery of unknown patterns or associated features in data.
![Unsupervised learning](./photos/unsupervisedLearning.png "Unsupervised Learning")

Unsupervised learning is divided into the following three categories:

- Correlation: It aims to discover meaningful relationships between variables in large data sets. It is widely used in market basket analysis to discover purchasing patterns among different products.
- Clustering: Clustering algorithms attempt to divide a data set into groups of similar objects without requiring pre-annotated training data. Its purpose is to find natural groupings in the data. Typical examples of clustering include K-Means and hierarchical clustering.
- Dimensionality reduction: Dimensionality reduction techniques are used to reduce the number of variables in a data set while still retaining important information. This helps improve model efficiency and performance while reducing computational costs. Dimensionality reduction can be achieved through principal component analysis[6].

(3)Reinforcement learning

Reinforcement learning is a field that sits at the intersection of machine learning, psychology, ethics, and information theory, and involves letting algorithms learn through trial and error in dynamic environments to achieve specific goals. In this process, the algorithm receives positive or negative feedback about its behavior and adjusts its action strategy based on this feedback to maximize positive feedback[7]. Unlike supervised learning, reinforcement learning does not rely on the provision of correct examples or the explicit correction of incorrect decisions. Its learning focus is on improving performance in real-world scenarios.

Reinforcement learning has applications ranging from developing agents that can beat human players in complex games to decision-making in self-driving cars. These use cases demonstrate the potential of reinforcement learning to solve problems that require taking multiple factors into account and learning to adapt to desirable behaviors in a changing environment.



机器学习是一种使计算机能够通过经验改进自身的技术，而不需要显式的编程指令[3]。 它是人工智能的一个子领域，旨在开发使计算机能够从数据中学习并做出决策或预测的算法。

机器学习算法一般分为三类：监督学习、无监督学习和强化学习。 监督学习算法通过训练包含输入和预期输出的数据集来学习。 无监督学习通过分析未标记的数据来发现数据的内在模式和结构，而强化学习则通过奖励和惩罚机制引导模型做出最优决策。[4]。

（1）监督学习
如下图所示在监督学习中，算法利用带有明确输出的示例输入数据（即训练数据）来学习如何将输入映射到输出。这个过程包括模型通过反复训练，对比其预测结果与实际结果，以此进行自我修正，直至达到高准确度。监督机器学习算法可以使用标记数据示例将过去学到的知识应用到新数据中来预测未来事件。监督学习问题可以采用以下形式：

分类：它属于特定类别的输出，例如将电子邮件分类为垃圾邮件或非垃圾邮件。这类任务通常使用预先标记的数据训练模型，并采用算法如支持向量机、朴素贝叶斯、k 最近邻等来构建决策边界，从而实现分类。
回归：它旨在预测连续数值的输出，例如房价或温度。与分类的主要区别在于回归的输出是连续值而非离散类别。常用的回归模型包括线性回归、多项式回归和支持向量回归。
预测：根据过去和现在的数据进行预测的方法。这也称为时间序列预测[4]。


(2)无监督学习

无监督学习是机器学习的另一种方式，它不依赖于预先标记好的数据，流程如下图所示。这种方法允许算法自行探索输入数据，尝试理解数据的内在结构和模式。由于在无监督学习中，数据没有附加的标签或类别信息，算法必须自己找出数据的组织方式，例如通过聚类相似的数据点、寻找异常值或通过其他方式理解数据集的特征。这种学习模式特别适合于数据探索和发现数据中未知的模式或关联特征。

无监督学习分为以下三类：

关联：它旨在发现大型数据集中变量之间的有意义的关系。它被广泛用于市场篮子分析，以发现不同产品之间的购买模式。
聚类：聚类算法试图将数据集分成由相似对象组成的群组，而不需要预先标注的训练数据。其目的是在数据中找到自然分组。聚类的典型例子包括K-均值算法(K-Means)和层次聚类(Hierarchical Clustering)​。
降纬：降维技术用于减少数据集中的变量数量，同时仍保留重要的信息。这有助于改进模型的效率和性能，同时减少计算成本。降维可以通过主成分分析法来实现。

(3)强化学习

强化学习是一个位于机器学习、心理学、伦理和信息论交叉点的领域，它涉及让算法在动态环境中通过试错学习以实现特定目标。在这个过程中，算法接收到关于其行为的正面或负面反馈，并根据这些反馈调整其行动策略以最大化正面反馈。与监督学习不同，强化学习不依赖于正确示例的提供或错误决策的明确纠正，它学习重点是在真实世界场景中提高性能。

强化学习的应用广泛，从开发能够在复杂游戏中击败人类选手的智能体，再到自动驾驶汽车的决策制定过程。这些应用案例展示了强化学习在解决需要考虑多种因素和学习适应不断变化环境中理想行为的问题上的潜力。





#### 2.1.2 Large Language Model

Large language model (LLM) is an advanced machine learning model that uses deep learning, especially the Transformer architecture, to understand and generate human language. This architecture is also the large language model architecture that we will mainly use and study in this project. They usually adopt unsupervised learning to pre-train large-scale text data sets to learn common language patterns, including grammar, vocabulary, and contextual information, so as to be able to process and analyze complex text data[5]. 

This training process gives LLMs powerful generative capabilities, enabling them to perform well on a variety of natural language processing tasks, such as text summarization, question answering systems, machine translation, and sentiment analysis. In particular, LLMs have shown their superiority in generating coherent, relevant text, which makes them useful in areas such as writing articles, generating creative content, and automating customer service conversations.At the same time, the interactivity of LLM allows users to interact with the model through natural language instructions, providing a foundation for applications such as virtual assistants and chat robots. 

(1)Transfomer architecture
Most large language models (LLM) such as the GPT (Generative Pre-trained Transformer) series rely on the Transformer architecture in deep learning. The transformer architecture was first proposed by Vaswani et al. in the 2017 paper "Attention is All You Need". It abandoned the CNN and RNN used in previous deep learning tasks. This architecture processes sequence data through the self-attention mechanism and position encoding, and can capture long-distance dependencies within the sequence [8].

As shown in the figure below, the overall structure of Transformer is the same as the Attention model. It also uses the encoder-decoder architecture. But its structure is more complex than Attention. Its encoder layer is stacked by 6 encoders, and the same is true for the decoder layer.
![Transformer](./photos/transformerGneral.png "Transformer")

i) Encoder and Decoder Stacks

The figure below shows the specific single-layer encoder (left half of the figure) and decoder (right half of the figure) structures. In each layer of the encoder, there is first a self-attention mechanism that can handle multiple attention heads simultaneously, followed by a simple positional fully connected feed-forward network. In order to enhance the capabilities of the model, residual connections [9] and hierarchical normalization technology are introduced, that is, the output of each sub-layer is normalized after adding the input [10]. The output dimensions of all sub-layers and embedding layers are unified to 512 dimensions to ensure the effectiveness of the residual connection.

The decoder is similar in structure to the encoder, but an additional sub-layer is added to the two basic sub-layers to implement multi-head attention processing of the encoder output. In addition, the self-attention mechanism in the decoder is specifically designed to prevent prediction of information about future positions. This is achieved through a masking technique that ensures that when generating each word, it only relies on the words that precede it. This design allows the model to build the entire sequence step by step based on previous context when generating text.
![Transformer](./photos/transformer.png "Transformer")

ii) Attention

The attention mechanism works by mapping a query and a set of key-value pairs to an output, where the query, key, value, and output are all in vector form. This mechanism determines a weighted sum of values by calculating the dot product of the query with all keys, dividing each dot product by √d_k and applying the softmax function. This process, called "scaled dot product attention," enables efficient calculation of weights given a d_k-dimensional query and key and a d_v-dimensional value. To speed up computation, a set of queries Q, keys K and values V are processed simultaneously, which are organized into matrices respectively. The output matrix calculation formula is: `Attention(Q, K, V) = softmax((QK^T)/√d_k)V`.
![Attention](./photos/attention.png "Attention")
Dot product attention and additive attention are two common implementation methods. Dot product attention is faster and more space-saving in practice because it can take advantage of efficient matrix multiplication. Additive attention performs better on an unscaled dot product basis when d_k is large, since large d_k values may cause the softmax function gradient to be extremely small. To solve this problem, the dot product results are scaled.

Multi-head attention allows the model to focus on different representation subspace information at different locations by performing different linear projections on queries, keys, and values, and executing attention functions in parallel on each projected version. This mechanism uses multiple parallel attention layers (heads), with the dimensionality of each head reduced, but the total computational cost is similar to single-head full-dimensional attention. The formula for this method is: `MultiHead(Q, K, V) = Concat(head_1, ..., head_h)W^O`, where `head_i = Attention(QW_i^Q, KW_i^K, VW_i^V) `. This enables the model to process complex information more efficiently.

iii) Feed-Forward Networks

In each layer of the Transformer, the position-by-point feedforward network independently applies the same two linear transformations to each position in the sequence, adding a ReLU activation function in the middle. This increases the model's ability to process non-linear data, allowing it to capture more complex features.

iv) Embeddings and Softmax

Responsible for converting input and output symbols (such as words) into vectors in high-dimensional space, and obtaining these vectors through learning. This enables the model to understand and process input and output data. For the output, a linear transformation and a softmax function are used to transform the output of the decoder into a probability distribution over the next symbol.


v) Positional Encoding

Positional Encoding provides the Transformer model with information about the position of each word in the sequence, which is achieved by adding position-based specific encoding to the word embedding, so that the model can recognize the order of words.


(2) Other NLP task architectures

Recurrent Neural Network (RNN): RNN is an early method for processing sequence data, which processes time series data through recurrent connections [11]. Although RNNs perform well when processing short sequences, they encounter the problem of vanishing or exploding gradients in long sequences.

Long short-term memory network (LSTM): LSTM is an improvement of RNN, which solves the vanishing gradient problem by introducing a gating mechanism (input gate, forget gate and output gate). LSTM can learn long-term dependencies and is widely used in natural language processing tasks [11].

Gated Recurrent Unit (GRU): GRU is another variant of RNN that simplifies the structure of LSTM, merges the input gate and forget gate into an update gate, and merges the unit state and hidden state. GRU is comparable to LSTM on some tasks, but is more computationally efficient.

Sequence-to-sequence (Seq2Seq) model: Seq2Seq models are commonly used in machine translation and other generative tasks. It contains two main parts: encoder and decoder [12]. The encoder processes the input sequence and the decoder generates the output sequence. The initial Seq2Seq model was built based on LSTM or GRU.

Convolutional Neural Networks (CNN): Although CNNs are primarily used for image processing, they are also used to handle natural language processing tasks, especially in text classification and sentiment analysis. CNN is able to capture local dependencies and patterns in text.


大型语言模型（LLM）是一种先进的机器学习模型，它使用深度学习，特别是 Transformer 架构来理解和生成人类语言，这个架构也是我们本次课题所要主要使用和研究的大语言模型架构。 他们通常采用无监督学习模式来对大规模文本数据集进行预训练来学习常见的语言模式，包括语法、词汇和上下文信息，从而能够处理和分析复杂的文本数据[5]。 

这一培训过程为LLMs提供了强大的生成能力，使他们能够在各种自然语言处理任务上表现出色，例如文本摘要、问答系统、机器翻译和情感分析。 特别是，法学硕士在生成连贯、相关的文本方面表现出了优势，这使得它们在撰写文章、生成创意内容和自动化客户服务对话等领域非常有用。同时，LLM的交互性允许用户通过自然语言指令与模型进行交互，为虚拟助手、聊天机器人等应用提供了基础。 

(1)Transfomer架构

大部分大型语言模型（LLM）如GPT（Generative Pre-trained Transformer）系列依赖于深度学习中的变换器（Transformer）架构。变换器架构首次由Vaswani等人在2017年的论文《Attention is All You Need》中提出，它抛弃了以往深度学习任务里面使用到的 CNN 和 RNN。这种架构通过自注意力（Self-Attention）机制和位置编码来处理序列数据，能够捕获序列内部的长距离依赖关系[8]。

如下图所示Transformer的总体结构和Attention模型一样，它也采用了encoer-decoder架构。但其结构相比于Attention更加复杂，它的encoder层由6个encoder堆叠在一起，decoder层也一样。

i) Encoder and Decoder Stacks

如下图所示为具体的单层encoder(图的左半部分)和decoder(图的右半部分)结构。在encoder的每一层中，首先是一个能够同时处理多个注意力头的自注意力机制，接着是一个简单的位置全连接前馈网络。为了增强模型的能力，引入了残差连接[9]和层级归一化技术，即每个子层的输出都会加上输入后进行归一化处理[10]。所有子层及嵌入层的输出维度统一为512维，以确保残差连接的有效性。

decoder构造上与encoder相似，但在两个基本子层外额外增加了一个子层，用于实现对编码器输出的多头注意力处理。此外，解码器中的自注意力机制被特别设计以防止对未来位置的信息进行预测，这是通过一个掩码技术实现的，确保在生成每个词时，只依赖于它前面的词。这种设计使得模型在生成文本时，能够一步步地基于之前的上下文构建出整个序列。

ii) Attention

注意力机制通过映射一个查询和一组键值对到一个输出，其中查询、键、值、和输出都是向量形式。该机制通过计算查询与所有键的点积，再将每个点积除以√d_k并应用softmax函数，来确定值的加权和。这种处理称为“缩放点积注意力”，能够在给定d_k维的查询和键及d_v维的值时，有效地计算权重。为了加速计算，同时处理一组查询Q、键K和值V，它们分别被组织成矩阵。输出矩阵计算公式为：`Attention(Q, K, V) = softmax((QK^T)/√d_k)V`。

点积注意力和加法注意力是两种常见的实现方式，其中点积注意力由于可以利用高效的矩阵乘法，因此在实际中更快、更节省空间。当d_k较大时，加法注意力在未缩放的点积基础上表现更好，因为大的d_k值可能导致softmax函数梯度极小。为了解决这个问题，点积结果会被缩放。

多头注意力通过对查询、键和值进行不同的线性投影，并对每个投影的版本并行执行注意力函数，允许模型在不同位置关注不同的表示子空间信息。这种机制采用多个并行的注意力层（头），每个头的维度减少，但总的计算成本与单头全维度注意力相似。这种方法的公式为：`MultiHead(Q, K, V) = Concat(head_1, ..., head_h)W^O`，其中`head_i = Attention(QW_i^Q, KW_i^K, VW_i^V)`。这使得模型能够更有效地处理复杂的信息。

iii) Feed-Forward Networks

在Transformer的每层中，位置逐点前馈网络对序列中每个位置独立应用相同的两次线性变换，中间加入ReLU激活函数。这增加了模型对数据的非线性处理能力，使其能够捕捉更复杂的特征。

iv) Embeddings and Softmax

负责将输入和输出的符号（比如单词）转换为高维空间中的向量，并且通过学习得到这些向量。这使模型能够理解和处理输入输出数据。对于输出，使用线性变换和softmax函数将解码器的输出转换成下一个符号的概率分布。


v) Positional Encoding

Positional Encoding 为Transformer模型提供序列中每个单词位置的信息，通过在词嵌入中加入基于位置的特定编码来实现，使模型能识别单词顺序。

（2）其他NLP任务架构

循环神经网络（RNN）：RNN是处理序列数据的一种早期方法，它通过循环连接来处理时间序列数据[11]。虽然RNN在处理短序列时表现良好，但它们在长序列中遇到梯度消失或梯度爆炸的问题。

长短期记忆网络（LSTM）：LSTM是RNN的一种改进，通过引入门控机制（输入门、遗忘门和输出门）来解决梯度消失问题。LSTM能够学习长期依赖关系，被广泛应用于自然语言处理任务[11]。

门控循环单元（GRU）：GRU是另一种RNN的变体，简化了LSTM的结构，将输入门和遗忘门合并为更新门，同时合并了单元状态和隐藏状态。GRU在某些任务上与LSTM相当，但计算更高效。

序列到序列（Seq2Seq）模型：Seq2Seq模型通常用于机器翻译和其他生成任务中。它包含两个主要部分：编码器和解码器[12]。编码器处理输入序列，解码器生成输出序列。初期的Seq2Seq模型基于LSTM或GRU构建。

卷积神经网络（CNN）：虽然CNN主要用于图像处理，但它们也被用于处理自然语言处理任务，尤其是在文本分类和情感分析中。CNN能够捕获局部依赖关系和文本中的模式。



### 2.2 Example analysis of user support and service automation


ChatGPT's design based on the Transformer model makes it an effective tool for a range of applications, including chatbots, language translation, and text generation. For example, National Australia Bank (NAB) used ChatGPT to develop a chatbot called "Mia" that can answer customer queries related to banking services, such as account balances, transaction history, and credit card details. This illustrates the practical application of ChatGPT in providing automated customer support and its wider potential across a variety of sectors.

Bard is also a chatbot developed by Google and powered by the LaMDA large language model [13]. LaMDA (Language Model for Dialogue Applications) is a language model based on Transformer architecture developed by Google. But unlike most language models, LaMDA’s training data includes dialogue, allowing it to capture how open dialogue differs from other language forms [14]. For example, it can understand the reasonableness and specificity of responses in the context of a conversation, which means that responses must not only make sense in the context of the conversation, but also be closely related to the content of the conversation.

The Quokka chatbot, fine-tuned on materials science-specific datasets using LLaMA large language models, demonstrates superior performance in user support in the materials science domain. It handles general queries proficiently, avoids sensitive topics appropriately, and demonstrates a strong ability to generalize various issues [15], embodying the ideal results of a user-supported system.

There are also new ways to use the Falcon-7B model to enhance e-commerce chatbots. Falcon-7B is an advanced large-scale language model with 7 billion parameters specifically designed to improve natural language understanding and generation. By training on a massive dataset, including 150 billion tokens from RefinedWeb and other curated corpora, Falcon-7B is able to perform computations efficiently without sacrificing scalability or performance [16]. In addition, 16-bit full quantization technology was used in the process of training this chatbot, which is a method of reducing numerical precision to reduce the memory and computing requirements of the model, making the model more suitable for running on resource-limited hardware [17]. Through these innovations, the application of the Falcon-7B model in e-commerce chatbots significantly improves the ability to understand user queries and generate relevant responses, thereby improving customer experience and potentially driving business growth.


ChatGPT 基于 Transformer 模型的设计使其成为一系列应用程序的有效工具，包括聊天机器人、语言翻译和文本生成。例如，澳大利亚国民银行 (NAB) 使用 ChatGPT 开发了一个名为“Mia”的聊天机器人，它可以回答客户与银行服务相关的查询，例如账户余额、交易历史记录和信用卡详细信息。这说明了 ChatGPT 在提供自动化客户支持方面的实际应用及其在各个领域的更广泛潜力。

同样Bard也是一个聊天机器人，它是由Google开发的，并由LaMDA大语言模型提供支持[13]。LaMDA（Language Model for Dialogue Applications）是Google开发的一种基于Transformer架构的语言模型。但与大多数语言模型不同的是，LaMDA的训练数据包括了对话，使其能够捕捉到开放式对话与其他语言形式的不同之处[14]。例如，它能够理解对话上下文中回应的合理性以及特定性，这意味着回应不仅要在对话情境下有意义，还应该与对话内容紧密相关。

Quokka 聊天机器人使用 LLaMA 大语言模型在材料科学特定数据集上进行了微调，在材料科学领域的用户支持方面表现出了卓越的性能。它能够熟练地处理一般查询，适当地避免敏感主题，并展示出强大的能力概括各种问题[15]，体现用户支持系统的理想结果。 

还有使用Falcon-7B模型来增强电商聊天机器人的新方法。Falcon-7B是一个先进的大型语言模型，拥有70亿参数，专门设计来改善自然语言理解和生成。通过在庞大的数据集上训练，包括来自RefinedWeb的1500亿个令牌和其他精选语料库，Falcon-7B能够高效地执行计算，同时不牺牲可扩展性或性能[16]。此外，在训练这个聊天机器人的过程中还使用了16位全量化技术，这是一种降低数值精度以减少模型的内存和计算需求的方法，使模型更适合于资源有限的硬件上运行[17]。通过这些创新，Falcon-7B模型在电商聊天机器人中的应用显著提升了理解用户查询和生成相关回应的能力，从而提高了客户体验并有潜力推动业务增长。

## 3. Transfer learning and model customization


Transfer learning is a machine learning method that allows a model to take knowledge learned on one task and apply it to another related but different task. This method is particularly suitable for situations where the amount of data is limited, because it can accelerate and optimize the learning process of new tasks by transferring existing knowledge [18]. Model customization refers to the process of adjusting and optimizing the pre-trained model according to specific needs based on transfer learning. In this process, the model may be fine-tuned based on our existing data sets or learning materials to make it Better adapted to a specific domain or task. I will mainly introduce two types of model fine-tuning below.


(1) Parameter efficient fine-tuning (PEFT)

PEFT is the latest technology, which only needs to modify a small part of the model parameters in the process of fine-tuning the model. It is currently the most popular method for adapting to large language models (LLM) [19]. Specifically, each task of large language model fine-tuning requires a completely different set of weights. When models scale to hundreds of billions of parameters, hosting a different set of weights for each model becomes very inefficient and cost-prohibitive, and reloading all the weights for different tasks is too slow. So the PETF technique aims to solve this problem by modifying a small portion of the weights relative to the entire model size while keeping the rest of the model frozen [20].

PETF methods can be broadly divided into three categories:
- Prefix/Prompt-Tuning: Add k additional trainable prefix tokens to the input or hidden layer of the model and train only these prefix parameters.
- Adapter-Tuning: Insert smaller neural network layers or modules into each layer of the pre-trained model. These newly inserted neural modules are called adapters. Only these adapter parameters are trained when fine-tuning downstream tasks.
- LoRA: Approximate the parameter update of the model weight matrix W by learning a low-rank matrix of small parameters, and only optimize the low-rank matrix parameters during training [21].

QLoRA is a variant of LoRA technology. Based on LoRA, QLoRA uses quantization technology to further reduce the resources and storage space required in the model fine-tuning process, thereby being able to fine-tune a 65B parameter model on a single 48GB GPU while maintaining a complete 16 Bit fine-tuning task performance. Research shows that QLoRA can achieve performance levels close to ChatGPT with just 24 hours of fine-tuning on a single GPU, marking a substantial advance in the accessibility and efficiency of fine-tuning LLMs [22]. This makes QLoRA particularly useful in resource-constrained situations, such as deploying large models in mobile devices or embedded systems. It mainly achieves optimization through the following points:

- Introducing 4-bit NF4 (Normal Float 4-bit) quantization to reduce the accuracy of model parameters and reduce video memory usage.
- Implement dual quantization, that is, quantize ordinary parameters and quantized parameters to further save storage space.
- Use low-rank adapter (LoRA) for fine-tuning, updating only a small number of parameters and keeping most of the pre-trained model parameters fixed.

(2) Reinforcement learning based on human feedback (RLHF)

It is a reinforcement learning technique that learns from human feedback. It adjusts the model's reward function through feedback provided by human evaluators, thereby optimizing model behavior [23]. The large language model (LLM) is mainly improved through the following three steps:

- Supervised fine-tuning (optional): First, a language model (LM) is initially fine-tuned to enable it to perform a specific task by using a supervised dataset containing input instructions and desired outputs. These data are usually manually annotated, covering a variety of tasks to ensure diversity.

- Train the reward model: Next, use human feedback data to train a reward model (RM). This step involves generating text outputs, inviting human annotators to evaluate the quality of these outputs, and ranking them according to preference. The reward model is then trained to predict human preference rankings of the outputs to reduce labeling costs. Advanced models such as GPT-4 can also be used to replace manual rankings.

- RL (reinforcement learning) fine-tuning: Finally, using the pre-trained LM as a strategy, the LM is further fine-tuned through the reinforcement learning (RL) method to make the text it generates more consistent with human annotation preferences. In the RL process, the reward generated by the reward model (RM) is used, and a penalty term may be introduced to avoid excessive deviation from the original model, such as using the PPO algorithm to optimize the LM and calculating the KL divergence as a penalty.

This process may go through multiple iterations to continuously optimize the model's output to make it more consistent with human expectations and instructions. With RLHF, large models are able to learn and improve their behavior based on human feedback, resulting in better and more relevant output when performing generative tasks.


迁移学习是一种机器学习方法，它允许模型利用在一个任务上学到的知识，应用到另一个相关但不同的任务上。这种方法特别适用于数据量有限的情境，因为它可以通过转移已有的知识来加速和优化新任务的学习过程[18]。模型定制是指在迁移学习的基础上根据特定需求调整和优化预训练模型的过程，在此过程中可能会基于我们已有的数据集或者学习资料对模型进行微调（Fine-tuning），使其更好地适应特定领域或任务。下面我将主要介绍两种模型微调种类。

(1)参数高效微调（PEFT）

PEFT是一类最新的技术，它在对模型微调的过程中只需要修改一小部分模型参数，是目前适应大型语言模型（LLM）最流行的方法[19]。具体来说，大语言模型微调的每项任务都需要一组完全不同的权重。当模型扩展到数千亿个参数时，为每个模型托管一组不同的权重变得非常低效且成本过高，而为不同任务重新加载所有权重又太慢。所以PETF技术旨在通过修改相对于整个模型大小的一小部分权重，同时保持模型的其余部分冻结来解决这个问题[20]。

PETF方法可大致分为三类：
- Prefix/Prompt-Tuning：在模型的输入或隐层添加 k 个额外可训练的前缀 tokens，只训练这些前缀参数。
- Adapter-Tuning：将较小的神经网络层或模块插入预训练模型的每一层，这些新插入的神经模块称为 adapter（适配器），下游任务微调时也只训练这些适配器参数；
- LoRA：通过学习小参数的低秩矩阵来近似模型权重矩阵 W 的参数更新，训练时只优化低秩矩阵参数[21]。

QLoRA是LoRA技术的一个变种，QLoRA在LoRA的基础上通过量化技术，进一步减少了模型微调过程中所需的资源和存储空间，从而能够在单个 48GB GPU 上微调 65B 参数模型，同时保持完整的 16 位微调任务表现。研究表明，QLoRA 只需在单个 GPU 上进行 24 小时的微调即可达到接近 ChatGPT 的性能水平，这标志着微调 LLM 的可访问性和效率有了实质性进步[22]。这使得QLoRA在资源受限的情况下尤其有用，例如在移动设备或嵌入式系统中部署大型模型。它主要通过以下几点实现优化：

- 引入4位的标准浮点数（Normal Float 4-bit, NF4）量化，以减少模型参数的精度，降低显存占用。
- 实施双重量化，即对普通参数和量化参数进行量化，进一步节约存储空间。
- 利用低秩适配器（LoRA）进行微调，只更新小部分参数，保持大部分预训练模型参数固定。

(2)基于人类反馈的强化学习（RLHF）

它是从人类反馈中学习的强化学习技术。它通过人类评价者提供的反馈来调整模型的奖励函数，进而优化模型行为[23]。主要通过以下三个步骤改进大型语言模型（LLM）：

- 监督微调（可选）：首先，通过使用包含输入指令和期望输出的监督数据集对语言模型（LM）进行初步微调，使其能够执行特定任务。这些数据通常由人工标注，覆盖各种任务以确保多样性。

- 训练奖励模型：接下来，使用人类反馈数据来训练一个奖励模型（RM）。这一步涉及生成文本输出，邀请人工标注员评价这些输出的质量，并根据偏好进行排序。奖励模型随后被训练以预测人类对输出的偏好排序，以减少标注成本，也可以利用如GPT-4这样的高级模型代替人工进行排序。

- RL（强化学习）微调：最后，使用预训练的LM作为策略，通过强化学习（RL）方法进一步微调LM，使其生成的文本更符合人类标注的偏好。RL过程中，使用奖励模型（RM）产生的奖励，同时可能引入惩罚项以避免与原始模型产生过大偏差，如使用PPO算法来优化LM，并计算KL散度作为惩罚。

这个过程可能会进行多次迭代，以不断优化模型的输出，使其更加符合人类的期望和指令。通过RLHF，大模型能够根据人类的反馈学习并改进其行为，从而在执行生成性任务时产生更优质、更相关的输出。




## 4. Data analysis

In this project, in addition to training and research on large language models, we can also focus on some other data processing and analysis methods and techniques to improve efficiency and performance of language models.

### 4.1 Intent recognition technology

By analyzing user questions, the chatbot can identify the intent behind the question and provide relevant answers accordingly. This process involves converting the question into vector embedding form and calculating the cosine similarity between the two vector embeddings to find the most relevant intents and tags. 

By predicting the intent of a user's query, Unklabot is able to more accurately understand the user's purpose in the first place. This involves extracting intent from users’ questions and using this information to generate relevant answers [24]. The system predicts the most relevant intent and its label by analyzing the cosine similarity between the user's question and intent.

通过分析用户的提问，聊天机器人可以识别隐藏在问题背后的意图，并据此提供相关回答。这一过程涉及将问题转换成向量嵌入形式，并计算两个向量嵌入之间的余弦相似性，以找出最相关的意图和标签​​。通过预测用户查询的意图，Unklabot能够在一开始就更准确地理解用户的目的。这包括从用户的问题中提取意图，并使用这些信息来生成相关的答案[24]。系统通过分析用户的问题和意图之间的余弦相似度来预测最相关的意图和其标签。

### 4.2 Semantic search technology

Semantic search matches appropriate content through the user's query intent, context, and conceptual meaning. When a user asks a question, the closest context or nugget of information can be returned even if there is no direct keyword match. Utilizing semantic search, Unklabot is able to understand the meaning behind user queries and retrieve the most relevant information, not just based on keyword matching. This approach not only provides context-sensitive and accurate responses, but also provides a deeper understanding of the user problem [24]​.

语义搜索是通过用户的查询意图、上下文和概念含义来匹配合适的内容。当用户提问时，即使没有直接的关键词匹配，也可以返回最接近的上下文或信息块。利用语义搜索，Unklabot能够理解用户查询背后的意义并检索最相关的信息，而不仅仅是基于关键词匹配。这种方法不仅提供了与上下文相关的准确响应，而且对用户问题有了更深入的理解​[24]​。

### 4.3 Semantic clustering

When users raise vague or incomplete queries, it is very important to effectively understand the user's intentions. We can use the idea of semantic cluster analysis to generate and ask targeted questions to guide the conversation, and ultimately provide the user with accurate information. required service information, thereby improving user experience and satisfaction. Therefore, this study [25] proposed the following steps and technical methods to implement the entire process:

- Text embedding processing: Use TF-IDF to convert service descriptions into numerical vectors so that machines can understand and compare different services.
- Apply clustering algorithm: cluster services through K-Means or DBSCAN algorithm to group similar services together.
- Select largest cluster: After clustering is complete, select the cluster that contains the largest number of services. Selecting the largest cluster helps the chatbot narrow its search more efficiently.
- Inference and generation of rhetorical questions: Perform in-depth analysis of the selected largest cluster and use semantic embedding technology to infer a superordinate term that can represent the entire cluster. By calculating the semantic similarity of each keyword in the cluster, the keywords that best represent the core meaning of the cluster are found. This keyword is used to generate clarifying rhetorical questions to the user to further identify the user's specific needs.
- User interaction and service recommendations: Based on the user's answers to rhetorical questions, the chatbot continues to refine the service selection until it finds the service that best meets the user's needs.

在用户提出模糊或不完整的查询时，有效地理解用户的意图是是十分重要的，我们可以通过语义聚类分析的思想来生成和提出针对性的问题来引导对话，最终精准地提供用户所需的服务信息，从而提升用户体验和满意度。所以此研究[25]，提出了以下步骤来和技术方法来实现整个流程：

- 文本嵌入处理：使用TF-IDF将服务描述转化为数值向量，使机器可以理解和比较不同服务。
- 应用聚类算法：通过K-Means或DBSCAN算法对服务进行聚类，把相似的服务分组在一起。
- 选择最大群集：在完成聚类后，选择包含服务数量最多的群集。选择最大的群集有助于聊天机器人更高效地缩小搜索范围。
- 推断和生成反问：对选定的最大群集进行深入分析，使用语义嵌入技术推断出一个能够代表整个群集的上级术语。通过计算群集中各关键词的语义相似性，找到最能代表群集核心含义的关键词。这个关键词被用来生成向用户提出的澄清性反问，以进一步确定用户的具体需求。
- 用户互动和服务推荐：根据用户对反问的回答，聊天机器人继续细化服务选择，直至找到最符合用户需求的服务。

### 4.4 Sentiment analysis technology

In such a study on public opinion comments on the Russia-Ukraine war [26], the author used the 20 most popular hashtags, and based on this, he crawled Twitter from a variety of different sources and related replies. information content, a custom data set was constructed. The data set includes tweet ID, tweet content, tweet replies and some question and answer data information, which has a similar structure to the data set we studied this time. On this basis, the author used random forest, naive Bayes and other methods to implement the task of sentiment analysis of text, and finally achieved a very smooth text sentiment analysis effect.

Therefore, sentiment analysis of users' language can also help us provide more personalized and emotionally appropriate responses, help us identify negative emotions or emergencies, and prioritize issues that require special attention, thereby improving user satisfaction and Interactive Experience. Similarly, we can also use sentiment analysis to analyze user feedback on our services to improve service functions and enhance user satisfaction.

在这样一项关于俄乌战争舆情评论的研究中[26]，作者使用了20个最流行的主题标签，并在此基础山在Twitter中抓取了多种不同来源的推文以及相关回复等信息内容，构建了一个自定义数据集。数据集包括推文ID、推文内容、推文回复以及一些问答数据信息，这与我们本次所研究的数据集具有类似的结构。在此基础上，作者使用随机森林、朴素贝叶斯等方法实现了对于文本的情感分析任务，最终得出了很不粗的文本情感分析效果。

所以对于用户的语言进行情感分析也可以帮助我们提供更加个性化和情感上合适的回应，帮助我们可以识别负面情绪或者紧急状况，去优先处理需要特别关注的问题，以此来提高用户满意度和互动体验。同样我们也可以使用情感分析去对用户对于我们服务的反馈进行分析，以此来改进服务功能，提升用户满意度。

### 4.5 Feedback and continuous learning mechanisms

This mechanism allows the chatbot to continuously learn and optimize based on user feedback and historical interaction data with users. By applying methods such as online learning, transfer learning, and reinforcement learning, chatbots can continuously adjust and improve their dialogue strategies to better meet the needs of users.

For example, a bot can identify and correct incorrect responses by analyzing user feedback, or learn new conversational strategies by observing user behavior patterns. This continuous learning process ensures that the chatbot continues to improve over time, providing more personalized and accurate user support.

这种机制允许聊天机器人根据用户的反馈和与用户的历史交互数据进行持续的学习和优化。通过应用在线学习、迁移学习和增强学习等方法，聊天机器人可以不断地调整和改进其对话策略，以更好地满足用户的需求。

例如，机器人可以通过分析用户反馈来识别和修正错误的回答，或者通过观察用户的行为模式来学习新的对话策略。这样的持续学习过程确保了聊天机器人随时间推进而不断进步，提供更加个性化和准确的用户支持。





[1]Arora, Ritu. "An introduction to big data, high performance computing, high-throughput computing, and Hadoop." Conquering Big Data with high performance computing (201ß6): 1-12.
[2] Koch, Miriam, et al. "HPC+ in the medical field: Overview and current examples." Technology and Health Care 31.4 (2023): 1509-1523.
[3]El Naqa, Issam, and Martin J. Murphy. What is machine learning?. Springer International Publishing, 2015.
[4]Bi, Qifang, et al. "What is machine learning? A primer for the epidemiologist." American journal of epidemiology 188.12 (2019): 2222-2239.
[5]Xi, Zhiheng, et al. "The rise and potential of large language model based agents: A survey." arXiv preprint arXiv:2309.07864 (2023).
[6]Rentzmann, Simon, and Mario V. Wuthrich. "Unsupervised learning: What is a sports car?." Available at SSRN 3439358 (2019).
[7]Wiering, Marco A., and Martijn Van Otterlo. "Reinforcement learning." Adaptation, learning, and optimization 12.3 (2012): 729.
[8]Vaswani, Ashish, et al. "Attention is all you need." Advances in neural information processing systems 30 (2017).
[9]He, Kaiming, et al. "Deep residual learning for image recognition." Proceedings of the IEEE conference on computer vision and pattern recognition. 2016.
[10]Ba, Jimmy Lei, Jamie Ryan Kiros, and Geoffrey E. Hinton. "Layer normalization." arXiv preprint arXiv:1607.06450 (2016).
[11]Sherstinsky, Alex. "Fundamentals of recurrent neural network (RNN) and long short-term memory (LSTM) network." Physica D: Nonlinear Phenomena 404 (2020): 132306.
[12]Palasundram, Kulothunkan, et al. "SEQ2SEQ++: A multitasking-based Seq2Seq model to generate meaningful and relevant answers." IEEE Access 9 (2021): 164949-164975.
[13]King, Michael R. "Can Bard, Google’s experimental Chatbot based on the LaMDA large language model, help to analyze the gender and racial diversity of authors in your cited scientific references?." Cellular and Molecular Bioengineering 16.2 (2023): 175-179.
[14]Thoppilan, Romal, et al. "Lamda: Language models for dialog applications." arXiv preprint arXiv:2201.08239 (2022).
[15]Yang, Xianjun, Stephen D. Wilson, and Linda Petzold. "Quokka: An Open-source Large Language Model ChatBot for Material Science." arXiv preprint arXiv:2401.01089 (2024).
[16]Almazrouei, Ebtesam, et al. "The falcon series of open language models." arXiv preprint arXiv:2311.16867 (2023).
[17]Luo, Yang, et al. "Enhancing E-commerce Chatbots with Falcon-7B and 16-bit Full Quantization." Journal of Theory and Practice of Engineering Science 4.02 (2024): 52-57.
[18]Neyshabur, Behnam, Hanie Sedghi, and Chiyuan Zhang. "What is being transferred in transfer learning?." Advances in neural information processing systems 33 (2020): 512-523.
[19]Pu, George, et al. "Empirical analysis of the strengths and weaknesses of peft techniques for llms." arXiv preprint arXiv:2304.14999 (2023).
[20]Mao, Yuning, et al. "Unipelt: A unified framework for parameter-efficient language model tuning." arXiv preprint arXiv:2110.07577 (2021).
[21]Hu, Edward J., et al. "Lora: Low-rank adaptation of large language models." arXiv preprint arXiv:2106.09685 (2021).
[22]Dettmers, Tim, et al. "Qlora: Efficient finetuning of quantized llms." Advances in Neural Information Processing Systems 36 (2024).
[23]Ouyang, Long, et al. "Training language models to follow instructions with human feedback." Advances in neural information processing systems 35 (2022): 27730-27744.
[24]Taju, Semmy Wellem, et al. "AI-powered Chatbot for Information Service at Klabat University by Integrating OpenAI GPT-3 with Intent Recognition and Semantic Search." 2023 5th International Conference on Cybernetics and Intelligent System (ICORIS). IEEE, 2023.
[25]Pilgrim, Jannis, et al. "Guiding Users by Dynamically Generating Questions in a Chatbot System." LWDA. 2022.
[26]Thakkar, Harsh, et al. "Sentiment and Statistical Analysis on Custom Twitter Dataset for 2022 Russo-Ukrainian Conflict." 2023 International Conference on Intelligent and Innovative Technologies in Computing, Electrical and Electronics (IITCEE). IEEE, 2023.