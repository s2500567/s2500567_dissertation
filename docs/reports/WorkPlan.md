# Work Plan

## Project Planning Gantt Chart
The figure below is a Gantt chart of this work plan, which briefly and intuitively introduces the entire task arrangement of this project.
![gantt](./photos/gantte.png "gantt")

## Project Initiation and Preliminary Research

### Time period：Now - April 15th

### Main work

- Conduct literature reading, review and comparison to determine which large language models will ultimately be used and compared.
- Preliminarily set up the basic environment we need to use.
- A preliminary look and understanding of the data set we are going to use.

- 进行文献阅读、回顾和比较，确定最终要使用和比较的大语言模型是哪几种。
- 初步搭建好我们所需要使用的基本环境。
- 对我们所要使用的数据集进行初步的查看和了解。

### Milestone

Complete an in-depth review of the current literature on large-scale language models, and identify and select 2-3 potential models for subsequent comparison and evaluation. Preliminarily set up the experimental environment to ensure the smooth progress of the experiment.

完成对当前大型语言模型相关文献的深入回顾，识别并选择2-3个有潜力的模型进行后续比较和评估。初步搭建实验环境，确保实验的顺利进行。

## Data preprocessing

### Time period：April 16th - April 21th

### Main work

- Perform preprocessing, such as cleaning and sorting, on the data sets we want to use, and convert the data into the format we need and store it.
- Ensure the uniformity of the data format and make some possible annotations on the data to ensure that the data can be suitable for fine-tuning our model.
- At this stage, there is no need to do in-depth data analysis. You only need to do basic processing and conduct an overall summary and structural pattern analysis of the data. Ensure data is available and meets requirements.

- 针对于我们所要用到的数据集进行预处理例如清洗和整理，将数据转化为我们所需要的格式存储起来。
- 要确保数据格式的统一和以及对数据做一些可能需要的标注，以确保数据可以适用于我们模型的微调。
- 在这一阶段中不需要做深入的数据分析，只需要做基本的处理并且对数据进行一个总体的总结和结构模式分析就可以。确保数据是可用的、符合要求的。

### Milestone

Completed the data preprocessing steps of data cleaning, formatting and necessary annotation, generated a data set with a consistent format, and had an overall structural understanding and summary of the data to ensure that it meets the model fine-tuning requirements.

完成了数据清洗、格式化和必要标注的数据预处理步骤，生成了一致格式的数据集，并对数据有一个总体的结构性认识和总结，确保其符合模型微调要求。

## Model fine-tuning and preliminary evaluation

### Time period：April 22th - May 21th

### Main work

- After verifying the feasibility of the experimental environment, start fine-tuning the selected model based on the data set collected by EPCC and conduct preliminary experiments.
- Use some existing model performance evaluation methods to evaluate the performance of the model, and initially select the best applicable model based on the results.
- Preliminarily consider further optimization strategies for the best model. For example, you can first roughly think about what aspects to optimize.

- 对实验环境进行验证可行性后，开始对选择的模型基于EPCC所收集的数据集做微调训练，进行初步的实验。
- 使用现有的一些模型性能评估方法对模型的性能做评估，并初步通过结果选择出最佳适用模型。
- 初步考虑对最佳模型的进一步优化策略，比如说可以先大致思考做哪些方面的优化。

### Milestone

Fine-tuning and preliminary experiments were conducted on the selected models, and a preliminary evaluation report containing performance comparisons of each model was produced, providing a basis for selecting the best model.

对选定模型进行了微调和初步实验，产出了一个包含各模型性能比较的初步评估报告，为选择最佳模型提供了依据。

## In-depth model optimization

### Time period：May 22th - June 14th

### Main work

- Based on the results of preliminary performance evaluation, determine which aspects of optimization can bring the greatest performance improvement. Possible optimization directions include adjusting the learning rate, introducing more complex loss functions (such as focal loss for category imbalance problems), or using data enhancement to improve the generalization ability of the model.
- Implement the selected optimization strategy, which may include, for example, coding a new loss function, adjusting model training parameters, or generating additional training data. At the same time, start applying any possible lightweight technology to prepare for mobile device adaptation.
- Run the optimized model for training and monitor performance metrics to ensure the effectiveness of the optimization strategy.

- 基于初步性能评估的结果，确定哪些方面的优化能带来最大的性能提升。可能的优化方向包括调整学习率、引入更复杂的损失函数（如焦点损失Focal Loss针对类别不平衡问题），或是采用数据增强来提升模型的泛化能力。
- 实施选定的优化策略，例如可能包括编码实现新的损失函数、调整模型训练参数、或是生成额外的训练数据。同时，开始应用任何可能的轻量化技术预备移动设备适配。
- 运行优化后的模型进行训练，并监控性能指标以确保优化策略的有效性。

### Milestone

Implemented targeted optimization strategies, including adjusting model parameters and using data enhancement technology, and completed a performance improvement report of the optimized model, showing the performance comparison before and after optimization.

实施了针对性的优化策略，包括调整模型参数和使用数据增强技术，完成了优化后模型的性能提升报告，展示了优化前后的性能对比。

## Mid-term performance review

### Time period：June 15th - June 20th

### Main work

- Use the test data set to perform performance evaluation of the optimized model, and collect and analyze key performance indicators such as precision, recall, and F1 score. Evaluate the model's generalization ability to new data and confirm whether the optimization achieves the expected results.

- 使用测试数据集对优化后的模型进行性能评估，收集和分析关键性能指标，比如准确率、召回率和F1分数。评估模型对新数据的泛化能力，确认优化是否达到预期效果。

### Milestone

A mid-term performance evaluation of the optimized model was conducted, the results containing detailed performance data and analysis were produced, and the next step of model improvement plan was determined.

对优化后的模型进行了中期性能评估，产出了包含详细性能数据和分析的结果，确定了下一步的模型改进计划。

## Advanced performance evaluation(optional)

### Time period：June 15th - June 30 th

### Main work

- Think about and integrate other nlp modules useful for this project into the system, such as speech recognition and output technology, and adjust them to suit the project data. Prepare test cases and data to ensure that sentiment analysis can correctly identify user emotions and feed them back to the main model.
- Run integration tests to ensure the model can adjust its answering strategy based on user sentiment. Collect test results and analyze the accuracy of the sentiment analysis module and its impact on the performance of the main model.

- 思考并集成其他对本项目有用nlp模块到系统中，例如语音识别和输出技术，并调整以适应项目数据。准备测试案例和数据，确保情绪分析能够正确地识别用户情绪并反馈给主模型。
- 运行集成测试，确保模型能够根据用户情绪调整回答策略。收集测试结果，分析情绪分析模块的准确性和对主模型性能的影响。

### Milestone

Integrated additional NLP functional modules to test the performance and adaptability of the completed model in handling more complex scenarios.

集成了额外的NLP功能模块，测试完成模型在处理更复杂场景时的性能和适应性。

## Mobile device adaptation(Optional)

### Time period：July 1st - July 15th

### Main work

- In-depth study of model lightweighting and optimization methods, such as model pruning, quantization, or knowledge distillation, to reduce model size and accelerate the inference process. Try implementing lightweighting techniques and try to test model performance in a simulated mobile device environment.
- Start conceiving the writing work of the paper and communicate with the tutor.

- 深入研究模型轻量化和优化方法，比如模型剪枝、量化或知识蒸馏，以减少模型大小和加速推理过程。尝试实施轻量化技术，并尝试在模拟的移动设备环境中测试模型性能。
- 开始构思论文的撰写工作，并与导师进行沟通。

### Milestone

Explore model lightweighting and optimization methods, complete mobile device adaptation, and ensure that the model can maintain good performance in resource-constrained environments.

探索模型轻量化和优化方法，完成移动设备适配，确保模型在资源受限的环境中也能保持良好性能。

## Paper writing

### Time period：May 22th - August 10th

### Main work

- Write the introduction and related work section of the paper to clarify the motivation, objectives and background of the research. Start organizing the experimental design and methodology sections to ensure that readers clearly understand the steps and rationale for the study.
- Write a results section based on the results of completed experiments and evaluations. Includes detailed data analysis, graphs and key findings. Begin writing the Discussion section, explaining the significance of the results, discussing the limitations of the study and potential directions for improvement.
- Complete the conclusion section of the paper, summarizing the main findings of the study and recommendations for future work. Review and revise the full text to ensure clear logic and accurate language. Be prepared to submit a draft of the paper for feedback.

- 撰写论文的引言和相关工作部分，明确研究动机、目标和背景。开始整理实验设计和方法论部分，确保读者能够清楚地理解研究的步骤和理由。
- 根据完成的实验和评估结果撰写结果部分。包括详细的数据分析、图表和关键发现。开始撰写讨论部分，解释结果的意义，讨论研究的限制和潜在的改进方向。
- 完成论文的结论部分，总结研究的主要发现和对未来工作的建议。进行全文的复查和修改，确保逻辑清晰、语言准确。准备提交论文草稿以获取反馈。

### Milestone

Completed the comprehensive writing of the research paper, including the introduction, methods, experimental results and discussion, and prepared relevant materials for project results presentation.

完成了研究论文的全面撰写，包括引言、方法、实验结果和讨论等部分，并准备了项目成果展示的相关材料。

## Additional information

The purpose of this work plan is to provide a preliminary and structured guide for the upcoming research project to ensure that the project objectives can be successfully completed within the specified time. This plan will be adapted based on the actual progress of the project and discussions with the mentor during the course of the project. This work rhythm of first tightening and then relaxing is aimed at completing core tasks and important milestones as early as possible, leaving ample room for maneuver for the project, and also ensuring that the project can move forward steadily when encountering unpredictable challenges.

For example, I set aside about 20 days as an additional buffer period in the later stages of the plan. This time can be used to deal with unforeseen problems, be flexibly allocated to early work projects, or further optimize and improve the project. . This arrangement not only provides the necessary flexibility for the project, but also allows for in-depth analysis and detailed thesis writing work.

Additionally, the two optional tasks noted in the work plan are not required for project success. They are designed as potential extensions of the project to further enhance the applied value and impact of the research results. If the early core work can be completed as planned and time permits, I will work on implementing these optional tasks. However, if the initial workload is underestimated or unexpected difficulties are encountered, ensuring that the main goals are achieved first will be my top priority.

本工作计划旨在为即将进行的研究项目提供一个初步且结构化的指南，以确保项目目标能够在规定的时间内顺利完成。这个计划将根据项目的实际进展和在项目进行过程中与导师的讨论进行调整。这种先紧后松的工作节奏旨在尽早完成核心任务和重要的里程碑，为项目留出充足的回旋余地，也为了确保在遇到不可预测的挑战时项目能够稳健前行。

例如我在计划的后期预留了大约20多天的时间作为额外的缓冲期，这段时间可以被用来应对未预见的问题、可以灵活分配给前期的工作项目或进行项目的进一步优化和完善。这样的安排不仅为项目提供了必要的灵活性，也允许进行深度的分析和细致的论文撰写工作。

此外，工作计划中标注的两项可选任务并非项目成功的必要条件。它们被设计为项目的潜在扩展方向，旨在进一步提升研究成果的应用价值和影响力。如果前期的核心工作能够按计划顺利完成，并且时间允许，我将致力于实现这些可选任务。然而，如果初期工作量被低估，或者遇到了预料之外的难题，优先保证主要目标的实现将是我的首要任务。







