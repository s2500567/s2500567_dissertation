# Project Proposal

## Project goals and objectives

This project aims to explore and implement advanced natural language processing (NLP) techniques, specifically the application of large language models (LLM), on HPC user support datasets to automate the processing of user queries.

 The initial goal of the project is to fine-tune several large-scale language models with the best performance at this stage (such as BERT, GPT-3 and LLaMA2), and then compare and analyze the results such as accuracy and response speed to select the best-performing model for deeper model optimization to improve overall performance. In addition, the project will also explore and try to integrate other NLP technologies for user sentiment analysis or problem clustering to provide more personalized and accurate user support. 


本项目旨在探索和实施先进的自然语言处理（NLP）技术，特别是大型语言模型（LLM）在HPC用户支持数据集上的应用，以自动化处理用户查询。

项目初步目标是将对现阶段性能最优的几个大型语言模型（如BERT、GPT-3和LLaMA2）进行微调，然后对结果例如准确性和响应速度进行比较分析，选出表现最佳的模型进行更深的模型优化，以提升整体性能。此外，项目还将探讨并尝试集成其他NLP技术进行例如用户情绪分析或问题聚类，以提供更加个性化和精准的用户支持。

## Methodology

### Model selection and comparison:

- Select several large-scale language models with the best current performance.
- Conduct preliminary fine-tuning training of these models based on the user support interaction data set collected by EPCC.
- Compare the effects of each model through performance evaluation indicators (such as accuracy, recall, F1 score and response time) and select the best performing model.

- 选择当前性能最优的几个大型语言模型。
- 基于EPCC收集的用户支持交互数据集，对这些模型进行初步的微调训练。
- 通过性能评估指标（如准确率、召回率、F1分数和响应时间）比较各模型的效果，选出表现最佳的模型。

### In-depth model optimization:

- Conduct in-depth fine-tuning and optimization of the selected best model, including but not limited to strategies such as loss function optimization, data enhancement, and model distillation.
- Explore the use of NLP technology for user emotion analysis, and guide the model's response strategy by understanding the user's emotional attitude to further improve user satisfaction.

- 对选定的最佳模型进行深入的微调与优化，包括但不限于损失函数优化、数据增强和模型蒸馏等策略。
- 探索使用NLP技术进行用户情绪分析，通过理解用户的情感态度来指导模型的响应策略，进一步提升用户满意度。

### Performance evaluation and tuning:

- Gain insights into model performance and user feedback using advanced performance evaluation methods including confusion matrices, user satisfaction surveys, and more.
- Iteratively tune the model based on the evaluation results until the expected performance target is achieved.

- 使用高级性能评估方法（包括混淆矩阵、用户满意度调查等）深入了解模型的表现和用户反馈。
- 根据评估结果对模型进行迭代调优，直至达到预期的性能目标。


## Success criteria 

Model performance: On the EPCC user support data set, the best selected model must demonstrate higher performance than competing models and preliminary fine-tuned models. This includes not only traditional NLP performance metrics (such as precision, recall, The improvement of F1 score) also includes the optimization of response time to ensure that users receive rapid feedback.

Personalized support: The model can provide personalized user support by analyzing the user's emotions and intentions. This means that the model can not only understand and answer technical questions, but also identify the user's emotional state to a certain extent and adjust the answer method according to the emotional state to improve user satisfaction.

Robustness and reliability: Models should demonstrate a high degree of robustness and reliability when faced with a variety of queries in the dataset, including effective handling of noisy data, non-standard usage, or novel queries.

模型性能：在EPCC用户支持数据集上，选出的最佳模型必须展现出比竞争模型和经过初步微调的模型更高的性能，这不仅包括传统的NLP性能指标（如准确率、召回率、F1分数）的提升，还包括响应时间的优化，以确保用户得到迅速反馈。

个性化支持：模型能够通过对用户的情绪和意图进行分析，提供个性化的用户支持。这意味着模型不仅能理解和回答技术问题，还能在一定程度上识别用户的情绪状态，根据情绪状态调整回答方式，以提高用户满意度。

鲁棒性与可靠性：在面对数据集中的各种查询时，模型应表现出高度的鲁棒性和可靠性，包括对噪声数据、非标准用法或新颖查询的有效处理。

## Minimum viable product (MVP)

The model, even under limited optimization conditions, should at least be able to automatically process and answer common, well-defined user queries in HPC services. This MVP should demonstrate the basic usability of the model for automated user support, even without achieving all expected functionality.

Even if the model is not fully personalized support, it should be able to recognize and respond appropriately to the most basic user emotions (such as satisfaction/dissatisfaction), thus demonstrating the potential of integrated NLP technology in improving user experience.

模型即使在有限的优化条件下，也应至少能够自动化处理和回答HPC服务中常见的、定义清晰的用户查询。这个MVP应能证明模型在自动化用户支持方面的基本可用性，即使在没有实现全部预期功能的情况下也是如此。

模型即使不是完全个性化的支持，它应能识别并对最基本的用户情绪（如满意/不满意）作出适当反应，从而证明集成NLP技术在提升用户体验方面的潜力。

## Alternative options

If the initially selected model fails to achieve the expected results after in-depth optimization, you should consider returning to the model selection stage and re-compare existing large language models based on new evaluation criteria. This may involve the evaluation of emerging models, or more in-depth testing of models that have not been fully considered previously. More integrated NLP techniques, such as automatic question classification, keyword extraction, etc., can be explored to find new optimization paths.

如果初始选定的模型在深入优化后未能达到预期效果，应考虑返回模型选型阶段，基于新的评估标准重新比较现有的大型语言模型。这可能涉及对新兴模型的评估，或是对先前未充分考虑的模型进行更深入的测试，可以探索更多集成NLP技术，如自动问题分类、关键词提取等，以寻找新的优化路径。

## Expansion direction

If the project progresses smoothly, expansion directions may include but are not limited to the following:

- Advanced interactive interface: Develop an advanced interactive interface that integrates speech recognition and speech generation technology, allowing users to make queries and receive answers through voice. This not only provides a more natural and convenient way of interaction, but also enables the system to serve visually impaired users or user groups who are more inclined to use voice interaction.

- Mobile device adaptation: Taking into account the diversity of user access services, the model is optimized so that it can run efficiently on mobile devices, thereby expanding the accessibility and convenience of the service.

- Personalized learning and feedback loop: Introducing machine learning and artificial intelligence algorithms to enable the system to learn from each user interaction and continuously optimize its performance and response strategies based on feedback. This continuous learning and adaptation mechanism will further enhance the intelligence level of the system and enable it to better meet the evolution of user needs.

若项目进展顺利，扩展方向可能包括但不限于如下方面：

- 先进的交互界面：开发集成了语音识别和语音生成技术的高级交互界面，允许用户通过语音进行查询和接收回答。这不仅提供了一种更自然和便捷的交互方式，也使得系统能够服务于视觉障碍用户或更倾向于使用语音交互的用户群体。

- 移动设备适配：考虑到用户访问服务的多样性，对模型进行优化，使其能够在移动设备上高效运行，从而拓展服务的可访问性和便利性。

- 个性化学习与反馈循环：引入机器学习和人工智能算法，使系统能够从每次用户交互中学习，并根据反馈不断优化其性能和响应策略。这种持续的学习和适应机制将进一步提升系统的智能化水平，使其能够更好地满足用户需求的演变。





