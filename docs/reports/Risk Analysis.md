# Risk Analysis

## Technology risk

### 1. Cirrus system down

**Description:** Cirrus system downtime may be caused by hardware failure, software errors or maintenance work. This will cause all computing tasks that rely on Cirrus to be forced to pause, thus delaying the project progress.

**Probability:** 10% (the system is generally stable, but accidental events are not ruled out)

**Potential delay:** 3 days (possible delays due to system recovery and rescheduling)

**Risk unit:** 0.10 * 3 = 0.3

**Priority:** Medium

**Mitigation measures:** Establish partnerships with multiple high-performance computing platforms such as archer2 and colab to ensure that at least one alternative platform is available. Back up all data and models regularly to ensure quick migration to other systems.

### 2. Insufficient GPU resource budget

**Description:** The time and complexity of model training can exceed budget, especially if model tuning or additional training rounds are required.

**Probability:** 40% (budget is usually sufficient, but there are uncertainties)

**Potential delay:** 3 days (possible delays caused by finding additional funding or optimizing resources)

**Risk units:** 0.40 * 3 = 1.2

**Priority:** Low

**Mitigation measures:** Negotiate with the supervisor to ensure GPU budget adequacy and allow additional budget headroom as a buffer. Explore resource optimization options, such as optimizing model structure to reduce computing load.

**3. EPCC data set issues**

**Description:** EPCC datasets may have data quality issues such as inconsistencies, incompleteness, or formatting issues, or may be difficult to access due to licensing and privacy issues.

**Probability:** 15% (datasets are generally reliable, but come with some risk)

**Potential delay:** 5 days (possible delay in finding alternative datasets and reprocessing data)

**Risk units:** 0.15 * 5 = 0.75

**Priority:** Medium

**Mitigation measures:**
- Conduct a comprehensive review of the dataset early in the project to assess data usability and quality. If possible, establish contact with the EPCC data management team so that data issues can be resolved promptly.
- Identify and prepare alternative public datasets that are similar in content and scale to the EPCC dataset. Develop a plan for rapid migration to an alternate data set and necessary preprocessing.

**4. Model performance is not up to standard**

**Description:** The model after initial adjustments may fail to meet the performance benchmark set by the project goals, and further adjustments or extended training cycles are required.

**Probability:** 50% 

**Potential delay:** 7 days 

**Risk units:** 0.5 * 7 = 3.5

**Priority:** Medium

**Mitigation:** Establish regular evaluation checkpoints to monitor model performance and ensure it is consistent with expected goals. Analyze performance bottlenecks and develop corresponding model improvement plans. This may include adjusting the model structure, changing the training strategy, or using more advanced algorithms.

## Other risks

### 1. Feeling unwell or sick

**Description:** An investigator is unable to continue working due to a sudden health problem, which may include an acute illness or other health problem.

**Probability:** 10% (maintain a healthy lifestyle, but cannot completely control it)

**Potential delay:** 7 days 

**Risk units:** 0.1 * 7 = 0.7

**Priority:** High

**Mitigation measures:** Create time buffers and flexibility into the project to ensure that if health issues are encountered, recuperation can be achieved without affecting the overall project progress. Additionally, maintain communication with instructors and team members to ensure they are informed and provide support when necessary.


### 2. Changes in project scope

**Description:** Research progress may lead to adjustments to the project scope, such as changes in research direction or new research questions.

**Probability:** 25% (the direction may be adjusted in the early stage of the project)

**Potential delay:** 10 days (possible delay as we assess impact and adjust plans)

**Risk units:** 0.25 * 10 = 2.5

**Priority:** Medium

**Mitigation measures:**
- Communicate regularly with instructors and project stakeholders to ensure clarity and consistency of project goals. Allow some flexibility in the project plan to accommodate possible adjustments.
- If the project scope needs to be adjusted, immediately assess the impact on resources, time, and budget and update the project plan accordingly.

