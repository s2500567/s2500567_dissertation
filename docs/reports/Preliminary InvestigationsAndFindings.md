# Preliminary Investigations and Findings

## Introduction

In the current social context, misunderstanding and neglect of mental health issues are widespread, further exacerbating the social stigma against these issues. Given the important impact mental health has on an individual's well-being and physical health, addressing this issue is particularly critical. Therefore, in order to improve public awareness and understanding of mental health issues, this project used a public mental health question and answer data set similar to the formal project in the early feasibility analysis stage to train a professional user support chatbot. The bot is designed to help people identify mental health issues they may be facing and provide initial suggestions for solutions.

The reason for choosing this small project for feasibility analysis is that it has many similarities with our formal project this time. For example, both projects use professional question and answer data sets and large language models in specific fields to train a Domain-specific user support and Q&A bots are dedicated to providing users with useful advice and solutions. So we believe that completing this small project can prove that formal project work is feasible and lay the foundation for formal project work.

在当前社会背景下，心理健康问题的误解与忽视普遍存在，进一步加剧了社会对这些问题的耻辱感。鉴于心理健康对个人的幸福和身体健康具有重要影响，解决这一问题显得尤为关键。因此，为了提高公众对心理健康问题的认识和理解，本项目在前期可行性分析阶段，使用了与正式项目相似的公开心理健康问答数据集来训练一个专业的用户支持聊天机器人。该机器人旨在帮助人们识别自己可能面临的心理健康问题，并提供初步的解决建议。

选用这个小项目进行可行性分析的原因是它和我们本次的正式项目具有很多方面的相似之处，例如两个项目都是利用特定领域的专业问答数据集以及大语言模型去训练一个针对于特定领域的用户支持和问答机器人，都致力于给用户提供有用的建议和解决方案。所以我们认为完成这个小项目能够证明正式的项目工作是可行的，并为正式项目的工作打下基础。


## Dataset collection

Since we did not obtain the official data set for this project, we chose an open source question and answer data set for model training. The data set was compiled from online FAQs, well-known medical websites such as Healthline, and other mental health-related articles. The data is preprocessed into a conversational format, including the patient's questions and the doctor's answers. This dataset can be found on the open source website "https://huggingface.co/datasets/heliosbrahma/mental_health_chatbot_dataset".

由于我们并未拿到本次项目的正式数据集，所以我们选择了一个开源的问答数据集去做模型训练。该数据集是从网上的常见问题解答、知名医疗网站如Healthline，以及其他心理健康相关的文章中整理出来的。数据预处理后形成了对话格式，包括病人的提问和医生的回答。这个数据集可以在“https://huggingface.co/datasets/heliosbrahma/mental_health_chatbot_dataset” 这个开源的网站找到。

## Model selection

For this small project, we chose the sharded Falcon-7b pre-training model. The Falcon-7b model has 7 billion parameters and a causal decoder architecture, making significant progress in natural language understanding and generation.

In addition, by sharding the model (that is, distributing different parts of the model on different servers or processors), memory consumption on a single device can be effectively managed and reduced, and sharding also allows different parts of the model to be run on multiple servers at the same time. run on the processing unit. This parallel processing greatly increases the speed and efficiency of data processing, especially when training on large data sets, which enables even systems with limited resources to handle large models.

针对于本次的小项目，我们选用了分片的Falcon-7b预训练模型。Falcon-7b模型拥有 70 亿个参数和因果解码器架构，在自然语言理解和生成方面有重大的进展。

除此外，通过将模型分片（即将模型的不同部分分布在不同的服务器或处理器上），可以有效管理和减少单个设备上的内存消，并且分片还允许模型的不同部分同时在多个处理单元上运行。这种并行处理极大地提高了数据处理的速度和效率，尤其是在大量数据集上进行训练时，这使得即使是资源有限的系统也能够处理大型模型。

## Experimental environment

This experimental environment is deployed on the Google Colab platform, which is a cloud service that provides online programming and deep learning experiments. In this model training, we used the Colab environment equipped with NVIDIA A100 GPU. NVIDIA A100 is based on the advanced Ampere architecture and has 6912 CUDA cores. It is specially designed for AI computing and high-performance computing (HPC), providing powerful computing power and efficient data processing performance.


本次实验环境部署在Google Colab平台，这是一个提供在线编程和深度学习实验的云服务。在这次模型训练中，我们使用了配备NVIDIA A100 GPU的Colab环境。NVIDIA A100是基于先进的Ampere架构，拥有6912个CUDA核心，专为AI计算和高性能计算（HPC）设计，提供了强大的计算能力和高效的数据处理性能。

## Technology

### QLoRA fine-tuning technology

QLoRA fine-tuning technology is an optimization technique that significantly reduces memory usage while preserving the performance of large language models (LLMs). It achieves efficient fine-tuning of large models on resource-limited hardware by quantizing model parameters and leveraging low-rank adapters (LoRA) for fine-tuning. Specifically, QLoRA significantly compresses the model size by using 4-bit NormalFloat (NF4) quantization technology and dual quantization method, while maintaining the performance of model fine-tuning through fine adjustment of LoRA parameters.

In NF4 quantization, each parameter no longer uses 32 bits (standard single-precision floating point numbers), but is represented by 4 bits, which greatly reduces the memory and storage requirements of the model. Although the information loss is greater, if the parameters themselves follow a normal distribution, this method can achieve parameter compression without significantly sacrificing model performance.

Dual quantization means that in addition to quantizing the parameters of the model, it also quantifies the quantification of these parameters themselves. This means that it not only compresses the model parameters, but also the representation of the parameters. The purpose of this approach is to reduce the size of the model as much as possible without sacrificing too much accuracy.
QLoRA微调技术是一种在保留大型语言模型（LLMs）性能的同时，显着减少内存使用的优化技术。它通过对模型参数进行量化和利用低秩适配器（LoRA）进行微调，以实现在资源有限的硬件上高效地微调大型模型。具体来说，QLoRA通过使用4-bit NormalFloat（NF4）量化技术和双重量化方法，显着压缩了模型大小，同时通过对LoRA参数的精细调整，保持了模型微调的性能。

在NF4量化中，每个参数不再使用32比特（标准的单精度浮点数），而是用4比特来表示，大大减少了模型的内存和存储需求。尽管信息损失更大，但如果参数本身服从正态分布，这种方法可以在不显著牺牲模型性能的情况下实现参数的压缩。

双重量化是除了将模型的参数量化之外，它还对这些参数的量化本身进行量化。这意味着它不仅压缩了模型参数，而且还压缩了参数的表示。这种方法的目的是尽可能在不牺牲太多精度的情况下进一步减少模型的大小。

 ### BitsandBytes library

BitsandBytes is a lightweight Python library that provides k-bit quantization for PyTorch. It mainly works around CUDA custom functions. It provides lightweight wrappers of CUDA custom functions for low-bit width operations such as 8-bit and 4-bit quantization.

When working with large models, reducing the bit-width of the weights from the standard 32-bit or 16-bit to a lower bit-width can significantly reduce memory usage, making it possible to run the model on devices with limited resources. So this time we use BitsandBytes to implement 4-bit quantization of the model to reduce memory usage and accelerate calculations.

BitsandBytes 是一个为 PyTorch 提供 k-bit 量化的轻量级Python库，它主要围绕 CUDA 自定义函数开展工作。它为诸如8位和4位量化这样的低位宽度操作提供了CUDA自定义函数的轻量级封装。

当处理大型模型时，将权重的位宽从标准的32位或16位减少到更低的位宽可以显著减少内存使用，从而使得模型在资源有限的设备上运行成为可能。所以我们本次使用BitsandBytes来实现模型的4位量化，以减少内存占用和加速计算。

### PEFT-LoRA

PEFT (Parameter-Efficient Fine-Tuning) is a technique for efficient fine-tuning of large pre-trained models. It adapts to specific downstream tasks by adjusting only a small part of the parameters in the model rather than all parameters. And LoRA is a method in PETF, and it is also the method we will use in this fine-tuning. It achieves efficient fine-tuning of parameters by adding a low-rank "update matrix" to specific parts of the model, rather than adjusting all parameters.
PEFT（Parameter-Efficient Fine-Tuning）是一种用于高效微调大型预训练模型的技术，它通过只调整模型中的一小部分参数而不是全部参数来适应特定的下游任务。

而LoRA是PETF中的一个方法，也是我们在本次微调中所要使用到的方法，它通过在模型的特定部分添加低秩“更新矩阵”来实现参数的高效微调，而不是对所有参数进行调整。

### wandb and TRL

We use wandb (Weights & Biases) to monitor various indicators during the model fine-tuning process, such as loss and accuracy, to help us visualize the training progress. TRL (Transformer Reinforcement Learning) is a method for training large language models (LLM). It uses reinforcement learning to optimize the output of the model to make it closer to human expected feedback. In the supervised fine-tuning step, we use TRL to adjust model parameters to improve performance on specific tasks, and wandb is used to track and visualize the effects of this process.

我们使用wandb（Weights & Biases）监控模型微调过程中的各种指标，如损失和准确率，帮助我们可视化训练进度。而TRL（Transformer Reinforcement Learning）是一种用于训练大型语言模型（LLM）的方法，它通过强化学习来优化模型的输出，使其更贴近人类的预期反馈。在监督微调步骤中，我们使用TRL来调整模型参数以提高特定任务的性能，而wandb则用于跟踪和可视化这一过程的效果。

## Training process

As shown in the code below, first load the sharded falcon-7b model, set 4-digit precision to load the model, and the loaded pre-trained model is in nf4 format. Set "bnb_4bit_use_double_quant" to True, which means using the double quantization mentioned in QLoRA.

如下图的代码所示，首先加载分片falcon-7b模型，设置4位精度加载模型，并且加载的预训练模型是nf4格式。设置“bnb_4bit_use_double_quant“为True，代表使用 QLoRA 中提到的双量化。
![model_load](./photos/model_load.png "model_load")

As shown in the figure below, when executing the text generation task, we set the task type to causal language model (CAUSAL_LM) and optimized the effect of LoRA by adjusting lora_alpha as the scaling factor of the weight matrix. 32 was chosen as the LoRA level, which is better in performance than settings with level 64 or 16. To maximize performance, I included almost all linear layers in the Transformer model in the LoRA target module, while setting the dropout rate (lora_dropout) of the LoRA layer to reduce overfitting.

如下图所示，在执行文本生成任务时，我们设置了任务类型为因果语言模型（CAUSAL_LM），并通过调整lora_alpha作为权重矩阵的缩放因子来优化LoRA的效果。选择了32作为LoRA的等级，这在性能上优于等级为64或16的设置。为了最大化性能，我将Transformer模型中几乎所有线性层包括在LoRA目标模块中，同时设置了LoRA层的丢弃率（lora_dropout）来减少过拟合。
![petf_setting](./photos/petf_setting.png "petf_setting")

As shown in the figure below, I used the SFTTrainer of the TRL library for fine-tuning. In order to balance the training speed, I set the maximum sequence length to 1024. Although increasing the sequence length can provide more contextual information, it will significantly reduce the training speed. Additionally, I tuned some key training parameters, such as batch size, gradient accumulation steps, type of linear scheduler used, and maximum number of steps.

If you encounter the problem of insufficient CUDA memory during training, an effective solution is to halve the batch size and double the number of gradient accumulation steps. This can reduce the memory required for each iteration while maintaining the total Training volume.

如下图，我使用了TRL库的SFTTrainer进行了微调，为了平衡训练速度，我设置了最大序列长度为1024，虽然增加序列长度能提供更多上下文信息，但会显著降低训练速度。此外，我还调整了一些关键的训练参数，比如批量大小、梯度累积步数、使用的线性调度器类型，以及最大步数。

如果在训练过程中遇到CUDA内存不足的问题，一个有效的解决方案是将批量大小减半，同时将梯度累积步数加倍，这样既可以减少每次迭代所需的内存，又能保持总的训练量。
![model_set](./photos/model_set.png "model_set")

## Training results

These plots show three key metrics of the model during training: loss, learning rate, and gradient norm. The loss chart shows that the loss continues to decrease with training steps, indicating that the model is continuously learning and improving its prediction accuracy. The learning rate chart reflects the decrease in learning rate over time, which is a preset decay strategy used to gradually reduce the step size as training progresses, helping the model find a more stable optimal solution. Fluctuations in the gradient norm graph may point to instability in parameter updates during training, and spikes indicate large parameter updates, possibly due to peculiarities of some batches of data. Excessively large fluctuations may require additional technical controls such as gradient clipping. Overall, the model training indicators indicate a normal convergence process, although the fluctuations in the gradient norm remind us to remain vigilant during the training process to ensure the stability of parameter updates.

这些图展示了模型在训练过程中的三个关键指标：损失、学习率和梯度范数。损失图表显示了损失随训练步骤持续下降，表明模型在不断学习并改进其预测准确性。学习率图表反映了学习率随时间递减，这是一个预设的衰减策略，用于随着训练的进展逐步减小步长，有助于模型找到更稳定的最优解。梯度范数图表的波动可能指出训练过程中参数更新的不稳定性，尖峰表明大的参数更新，可能由于某些批次数据的特殊性造成。过大的波动可能需要额外的技术控制如梯度裁剪。整体上，模型训练指标表明了一个正常的收敛过程，尽管梯度范数的波动提醒我们在训练过程中需保持警惕，确保参数更新的稳定性。

![train](./photos/W&B%20Chart%2012_03_2024,%2000_28_04.png "train")

![train](./photos/W&B%20Chart%2012_03_2024,%2000_27_44.png "train")

![train](./photos/W&B%20Chart%2012_03_2024,%2000_27_24.png "train")

When evaluating the final model for providing user support for mental health issues, we analyzed performance by comparing the responses generated by the PEFT model to the original sharded model. From the provided examples of questions and model responses, it can be observed that the PEFT model performs better at reducing the generation of hallucinations (unrealistic or irrelevant responses) and is able to provide more coherent and consistent responses.

This phenomenon shows that the fine-tuned trained model has been able to perform specialized learning based on a specific data set, effectively providing high-quality user support. In addition, this also implies that fine-tuning training is crucial to improving the performance of chatbots in specific field applications, and also illustrates the basic feasibility of fine-tuning training for this completed task.

在评估针对心理健康问题提供用户支持的最终模型时，我们通过比较PEFT模型与原始的分片模型生成的回答来分析性能。从所提供的问题和模型回答的示例中可以观察到，PEFT模型在减少生成幻觉（不切实际或不相关的回答）方面表现更佳，并且能够提供更加连贯、一致的响应。

这一现象表明，经过微调训练的模型已经能够根据特定的数据集进行专业化学习，有效地提供了高质量的用户支持。此外，这也暗示了微调训练对于提高聊天机器人在特定领域应用中的性能是至关重要的，也同样说明了微调训练对于本次毕设任务的基本可行性。
。![QA](./photos/QAresult.png "QA")
![QA](./photos/QAresult2.png "QA")













