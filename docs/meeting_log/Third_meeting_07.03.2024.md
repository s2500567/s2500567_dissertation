

# Basic Information

- **Date**: March 7, 2024
- **Participants**:
  - Adrain Jackson 
  - Daniyal Arshad 
  - Yuyang Zhou 
  - Hao Chu

# Overview of Meeting Content

## Introduction to EPCC Data Collection

- **Daniyal** introduced the extensive user support interaction data that EPCC has collected for the HPC services they operate. This data primarily consists of detailed exchanges between users and experts concerning specific issues and topics. The message groups are further enriched by associated metadata, providing a rich source of information for analysis and improvement of user support services.

## Review of User Support Interaction Cases

- **Daniyal** requested a thorough review of existing cases related to user support interactions. He specifically emphasized the importance of exploring how Learning Management Systems (LLMS) can be effectively utilized to enhance user support interactions, suggesting a potential area for improving service delivery and user satisfaction.

## Data Structure and Project Preparation

- **Yuyang** discussed the details of the server core project that he had uploaded on GitLab, questioning whether the level of detail was sufficient. He brought up considerations regarding the data structure, including aspects of server configurations and project management, indicating the complexity and the need for meticulous planning in project execution.

## Report Writing

- **Adrian** addressed the aspects of report writing, indicating that the literature review section should be based on a comprehensive and multi-faceted examination and analysis of the project. This might involve comparing multiple models or employing different methodologies within a single model. Subsequently, the focus should shift to a detailed introduction of the work conducted during the project timeline, emphasizing the need for clarity in reporting one's own contributions and findings.

## Model Selection and Adjustments

- **Yuyang** discussed the flexibility in model selection, proposing that it is possible to change the chosen model based on its performance during the project. He stressed the importance of clearly explaining the reasons behind any changes, suggesting that transparency in decision-making processes is critical for the credibility of the project.

## Project Progress and GitLab Repository Updates

- **Adrian** explained that the primary purpose of GitLab is to encourage frequent updates on one's work, rather than uploading all files in a short time span. He also mentioned that the structure of the GitLab repository is customizable, emphasizing that as long as it is logical, the specific structure is flexible.

## Data Use and Alternative Approaches

- **Daniyal** and **Yuyang** discussed the possibility of using dummy data or manually created user interaction data while waiting for the actual data. They highlighted that it is essential to focus on understanding the reasons behind any methods that do not work as expected and to document these in the report.

## Project Report and Evaluation

- **Adrian** noted that when writing the project report, it is crucial to detail why a specific model was chosen and to provide clear reasons if a change in the model is necessary. He also discussed setting minimum delivery standards and the importance of adjusting project targets flexibly throughout the project process.

# Conclusion of the Meeting

- The meeting concluded with constructive discussions, leaving all participants with a clearer understanding of the project's future direction and strategies. The dialog fostered a collaborative atmosphere, setting the stage for effective project execution and innovative solutions to the challenges identified.