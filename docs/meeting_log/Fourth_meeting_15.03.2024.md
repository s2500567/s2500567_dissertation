# Basic Information

- **Date**: March 15, 2024
- **Participants**:
  - Adrain Jackson 
  - Yuyang Zhou 

# Meeting Content

This meeting mainly asked Adrian to help me check the complete status of each part of the report that needs to be submitted for Project Preparation and the parts that need to be modified.

## Discussion Summary
- **New Dataset**
  - It was decided that a new, more comprehensive dataset will be prepared for analysis and as a backup. The specific details regarding the dataset's attributes and the sources from which the data will be collected were discussed.
  
- **Report Requirements**
  - Images used in the article need to be cited.
  - Milestones need to be added to the Work Plan document to determine what the completion results of each stage are.
  - Integrate various parts of the report into one report, and then make in-depth modifications to it
  
- **Work Plan for Data**
  - The data processing part of the Work Plan: Quick inspection and evaluation datasets are diverse and in-depth and can be used

