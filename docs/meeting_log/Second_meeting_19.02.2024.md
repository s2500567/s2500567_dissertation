
# Basic Information

- **Date**: Feb 19, 2024
- **Participants**:
  - Adrain Jackson 
  - Daniyal Arshad 
  - Yuyang Zhou 
  - Hao Chu

# Questions

## 1. Do we need to start some preparations? For example, setting up a coding environment or data processing, etc.
Yes, it's essential to first identify the model we plan to use. In the next meeting, we will provide the relevant data formats, which will then allow us to further establish the overall framework and the coding environment.

## 2. I am a little confused about the direction and final output of this project.
This project aims to develop a chat model that can automatically train a large language model based on the questions asked by users. It will reply to the users' questions and engage in conversations.

## 3. What progress needs to be made before the next meeting?
We should review different models and compare them to decide which one to use. It's possible to start with multiple models and then refine our selection. At the same time, we should closely examine the project's requirements and research what models are used by others for similar issues. This approach will also apply here.
