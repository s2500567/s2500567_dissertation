## How to evaluate the model ? what metrics I can use?

### 《Automating Customer Service using LangChain》

It is relatively difficult to evaluate LangChain agents,
especially when trained on large chunks of context datasets for
information retrieval. Hence, the current solution for the lack of
metrics is to rely on human knowledge to get a sense of how
the chain/agent is performing.

评估 LangChain 代理相对困难，尤其是在对大量上下文数据集进行信息检索训练时。因此，目前解决指标不足的方法是依靠人类知识来了解链/代理的运行情况。

A simple chat window can be activated at the corner of any
website which would enable users to interact with the chatbot
and ask any relevant questions or doubts regarding the
organization. However, for the demonstration purpose of this
paper, we are using Gradio API framework [7].

任何网站的角落都可以激活一个简单的聊天窗口，让用户与聊天机器人互动，并询问有关该组织的任何相关问题或疑问。但是，出于本文的演示目的，我们使用 Gradio API 框架 [7]。

###  《EduChat: A Large-Scale Language Model-based Chatbot System for Intelligent Education》

However, there are several challenges of applying LLMs into education domain. One challenge
(C1) is that there is still a gap between the LLMs
and the educational expert since LLMs are pretrained on the general corpus, which lack sufficient
educational knowledge and can not align well with
real scenarios (e.g., essay assessment). The other
challenge (C2) is that the knowledge in the field of
education is updating, while LLMs can not learn
up-to-date knowledge due to the training mechanism. Moreover, LLMs suffer from the hallucination problem, and may generate responses that are not truthful.
然而，将 LLM 应用于教育领域面临诸多挑战。一个挑战（C1）是 LLM 与教育专家之间仍然存在差距，因为 LLM 是在通用语料库上进行预训练的，缺乏足够的教育知识，不能很好地与真实场景（例如论文评估）保持一致。另一个挑战（C2）是教育领域的知识在不断更新，而 LLM 由于训练机制而无法学习最新的知识。此外，LLM 还存在幻觉问题，可能会产生不真实的反应。