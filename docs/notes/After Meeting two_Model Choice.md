## 1. **BERT (Bidirectional Encoder Representations from Transformers)**

### Pros：
1. **Deep Semantic Understanding**: BERT can better understand the relationship between words and the contextual meaning of text through the pre-trained two-way Transformer structure, making it outstanding in understanding complex sentence structure and meaning.
	[[https://hitchhikers.yext.com/blog/bert-strengths-and-weaknesses-34eabc48587b]]
2. **Applicable to a variety of NLP tasks**: BERT can be applied to a variety of NLP tasks, such as text classification, sentiment analysis, named entity recognition, etc., improving the versatility and flexibility of the model.
3. **Parallel Computing Capability**: Compared with previous models, BERT supports more efficient parallel computing through the self-attention mechanism (Attention Mechanism), which helps accelerate the training and inference process.

### Version：
1. BERT-Base：The Base version contains 12 layers of Transformer structure
2. ==BERT-large：The Large version contains a 24-layer Transformer structure==

### Derived model：
### A. RoBERTa (Robustly Optimized BERT Approach)
ALBERT addresses the memory consumption issue of BERT by factorizing the embedding matrix into two smaller matrices and sharing parameters across layers. It introduces two novel techniques: cross-layer parameter sharing and sentence-order prediction, which help in reducing model size while maintaining or even improving model performance.

### B.ALBERT (A Lite BERT)

ALBERT addresses the memory consumption issue of BERT by factorizing the embedding matrix into two smaller matrices and sharing parameters across layers. It introduces two novel techniques: cross-layer parameter sharing and sentence-order prediction, which help in reducing model size while maintaining or even improving model performance.

### C.ERNIE (Baidu's Enhanced Representation through kNowledge Integration)

ERNIE, developed by Baidu, enhances the representation of language models by integrating external knowledge such as entities, phrases, and semantic information. It demonstrates significant improvements in various NLP tasks, particularly those requiring understanding of complex semantics or real-world knowledge.

### D. XLNet

XLNet combines the best of BERT and autoregressive language modeling by capturing bidirectional context and using a permutation-based training strategy. This allows it to outperform BERT on several NLP tasks, especially those requiring a deep understanding of context and language structure.


## 2. ChatGPT
Both [BERT](https://blog.google/products/search/search-language-understanding-bert/) and [ChatGPT](https://openai.com/blog/chatgpt) require fine-tuning to achieve high accuracy on specific tasks. While [BERT](https://blog.google/products/search/search-language-understanding-bert/) is known for its speed and ability to handle short input sequences, it has limitations in its context understanding and text generation capabilities. ==On the other hand, [ChatGPT](https://openai.com/blog/chatgpt) excels in text generation and handling long sequences. Still, it is slower and has higher memory requirements.==
[BERT](https://blog.google/products/search/search-language-understanding-bert/) is a pre-trained model that uses a combination of masked language modeling and next-sentence prediction to understand the context of words in a sentence. It is commonly used for classification tasks like sentiment analysis and question answering. ==[ChatGPT](https://openai.com/blog/chatgpt) is a language model that uses autoregressive language modeling to generate text based on a given prompt. It is commonly used for tasks that require generating natural language responses, such as chatbots and language translation.==
（[[https://pareshmpatel.com/bert-or-chatgpt-the-pros-and-cons/]]）

|Features|BERT|ChatGPT|
|---|---|---|
|Pre-training Method|==Masked Language Model and Next Sentence Prediction== |==Autoregressive Language Modeling== |
|Fine-tuning Required|Yes|Yes|
|Context Understanding|==Limited== |==Strong== |
|Text Generation|==Not ideal== |Ideal|
|Ability to Complete Tasks|Good|Excellent|
|Speed|==Fast== |==Slow== |
|Training Time|Less|More|
|Memory Requirements|==Low== |High|
|Accuracy|==High== |Very High|
|Pretrained Models Available|Many|Few|
|Multilingual Support|Yes|Yes|
|Ability to Handle Long Sequences|==Poor== |==Good== |
|Quality of Generated Text|Fair|Good|
|Ability to Handle Multiple Inputs|No|Yes|
|Performance on Specific Tasks|Varies|Varies|
|Use in NLP|Common|Very common|
|Cost|Free|Expensive|
|Training Data Requirements|High|Very High|
## 3.LLaMA and LLaMA2(Transformer)
1. LLaMA Performance: LLaMA-13B outperforms GPT-3 (175B) on most benchmarks, and LLaMA65B is competitive with the best models, Chinchilla-70B and PaLM-540B.
2. But LLaMA 2 performance better. Llama 2 models are trained on 2 trillion tokens and have double the context length of Llama 1. Llama Chat models have additionally been trained on over 1 million new human annotations. ==The open source nature of LLaMA2 means that project teams can freely access and use the model ==(https://llama.meta.com/llama2)	
3. WHY LLaMA2?
	1. LLaMA2 provides model versions of different sizes, including models with 7B, 13B and 70B parameters
	2. LLaMA2 is very good at processing natural language, can generate high-quality text in a very short time, and can answer various questions and provide relevant information.
	3. ==LLAMA2 adopts optimisation measures such as pre-normalisation and SwiGLU activation function to show excellent performance in common sense reasoning and knowledge.==
	4. But ==compared with GPT-4 and Google's Pathways Language Model (PaLM) 2 (the basis for the Bard chatbot), the performance gap of Llama 2 is large.==
## 4. PaLM2（transformer）：Bard model
1. Multilingual capabilities: PaLM 2 has greatly improved its ability to understand, generate and translate text in different languages through training in more than 100 languages. This allows Bard to more accurately understand and respond to user queries in different languages
2. It excels at advanced reasoning tasks, including code and math, classification and question answering, translation and multilingual proficiency, and natural language generation better than our previous state-of-the-art LLMs, including PaLM. It can accomplish these tasks because of the way it was built – bringing together compute-optimal scaling, an improved dataset mixture, and model architecture improvements.（[[https://ai.google/discover/palm2/]]）

https://ai.google/static/documents/palm2techreport.pdf
