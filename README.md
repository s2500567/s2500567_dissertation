# LLMs for User Support

## Project Information

- **Project Title**: LLMs for User Support
- **Student**: Yuyang Zhou
- **Supervisors**: Adrian Jackson, Daniyal Arshad

## Description

This MSc project explores the use of Large Language Models (LLMs) such as ChatGPT and Bard for automating user support interactions within High-Performance Computing (HPC) services. With a substantial dataset of user support interactions, including message exchanges and associated metadata, this project aims to create a custom LLM. The project will involve data preprocessing, LLM transfer learning, and evaluation of results to investigate the feasibility of automated user support. Additional research areas might include sentiment analysis of user interactions and clustering of common issues.

## Getting Started

To get started with this project, you need to follow the setup instructions listed below. These will guide you on how to preprocess the data, train the LLM, and evaluate its performance.

### Prerequisites

- List of necessary software and tools, e.g., Python, specific libraries, etc.
- Any required API keys or access tokens.

## Repository Structure

### `/data`（not used yet）
Contains raw and processed datasets. Ensure that no sensitive or proprietary data is uploaded.
- `raw/`: The unprocessed original data.
- `processed/`: The cleaned and processed data.

### `/src`
The source code directory that includes scripts for data processing and model training.


### `/docs`
Documentation directory containing project reports and related documents.
- `reports/`: Drafts and final versions of project reports.
- `meeting_log/`: Meeting logs with supervisor.
- `pdfs/`: Including realted some papers.
- `notes/`: Including other learning notes.


### `/scripts`(not used yet)
Utility scripts, such as those to install dependencies or run models.


### `/results`(not used yet)
Outputs and evaluation results of the model, including graphs and performance metrics.

### Files in the Root Directory
- `README.md`: The main project information and a quick start guide.
- `.gitignore`: Specifies the files and directories to be ignored by git.
- `.gitlab-ci.yml`: Configuration file for GitLab CI/CD.

